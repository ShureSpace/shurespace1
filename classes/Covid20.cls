public with sharing class Covid20 {
    public object Covid20() {
        Http httpEmail=new Http();
            HttpRequest reqEmail=new HttpRequest();
            reqEmail.setendpoint('https://api.covid19india.org/states_daily.json');
            reqEmail.setHeader('Content-Type','application/json') ;
            reqEmail.setmethod('GET');
            //reqEmail.setbody(strBodyEmail);
            HttpResponse resEmail;
            resEmail = httpEmail.send(reqEmail);
            String strEmail=resEmail.getbody();
            system.debug('****Response from Contact keys API'+strEmail);
            //String strEmail = '{ "channelAddressResponseEntities": [ { "contactKeyDetails": [ { "contactKey": "0031I000015KipsQAC", "createDate": "2020-02-27T10:31:00" }, { "contactKey": "00Q1I00000LH4HCUA1", "createDate": "2020-01-21T08:45:00" } ], "channelAddress": "amason@amasongroup.com" }, { "contactKeyDetails": [ { "contactKey": "0031I000014LaNCQA0", "createDate": "2020-02-27T10:31:00" }, { "contactKey": "00Q1I00000LG9LBUA1", "createDate": "2020-01-18T09:13:00" } ], "channelAddress": "freddieg@earthlink.net" } ], "requestServiceMessageID": "d6d5171a-194c-4bd5-99f3-bb9ade8b26bc", "responseDateTime": "2020-03-10T21:34:27.8530797-06:00", "resultMessages": [], "serviceMessageID": "07c2c727-b0d8-4d9c-9e05-9589627d60c3" }';
            Covid20APIDetails Covid20APIDetailsResult = new Covid20APIDetails();
            Covid20APIDetailsResult = (Covid20APIDetails)JSON.deserialize(strEmail, Covid20APIDetails.class);
            system.debug(Covid20APIDetailsResult.States_daily);
            return Covid20APIDetailsResult;
    }
}