/**
 *This is wrapper class for knowledge article response.
 *<p/><p/>
 *@author Sachin
 */
public class KnowledgeArticleResponseWrapper_ACE {
    
    //Getters and Setters.
    @AuraEnabled public Boolean boolSuccess {get;set;}
    @AuraEnabled public String strErrorNumber {get;set;}
    @AuraEnabled public List<KnowledgeArticleWrapper_ACE> lstKnowledgeArticleWrapper {get;set;}
}