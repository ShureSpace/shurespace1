public class LoginPages {
 
    public String password { get; set; }
    public String username { get; set; }
    
    public PageReference registerUser() {
        PageReference newPage = new PageReference('/apex/shopnowreg');
        newPage.setRedirect(true);
        return newPage;
    }
    
    public PageReference login() {
        PageReference newPage = new PageReference('/apex/shopnow');
        newPage.setRedirect(true);
        return newPage;
    }
    
    public PageReference cancel() {
        PageReference newPage = new PageReference('/apex/loginshop');
        newPage.setRedirect(true);
        return newPage;
    }
}