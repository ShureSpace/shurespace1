/** 
*This calss serves as controller for searchcontactpage_ACE visualforce page. 
*<p/><p/>
*@author Pramod
*/
public class searchcontactclass_ACE {
    public boolean isSaveButtonDisabled = false;
    public boolean isNewButtonDisabled = true;
    public String Linktocontact { get; set; }
    public string searchboxstring{get; set;}
    public Account oneele{get;set;}
    public string res1{ get; set; }
    public list<contact> result {get; set;}
    public list<Account> acc { get; set; }
    /** 
     *This method is used for representing getIsNewButtonDisabled.
     *
     *@return Return the IsNewButtonDisabled on boolean format.
     */
    public boolean getIsNewButtonDisabled() {
        return IsNewButtonDisabled;
    }
    
    /**
     *This method is used for representing IsSaveButtonDisabled.
     *
     *@return Return the IsSaveButtonDisabled on boolean format.
     */
    public boolean getIsSaveButtonDisabled() {
        return IsSaveButtonDisabled;
    }
    /**
     *Constructor of the class.
     */
    public searchcontactclass_ACE()
    {
        result = new list<Contact>();
        acc = new list<account>();
    }
    /**
     *This method will  perform search and call callout2 method which perform callout.
     */
    public void searchbox(){
        String j= '%'+searchboxstring+'%';
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'firstname is required');
        result=new List<Contact>();
        result=[select ID,Name,lastname,firstname from Contact where name LIKE :j];
        callout2();
        if(result==null){}
        else
            IsNewButtonDisabled=false;
        Linktocontact='https://pramodreddy-dev-ed.my.salesforce.com/result.id';
    }
    /**
     *This method is used for creating new contact.
     *
     *@return Return the pg of type pagereference.
     */
    public pagereference createnew(){
        pagereference pg=new pagereference('/003/e?con2_ileinner='+searchboxstring);
        return pg;
    }
    /**
     *This method will perform the callout
     */ 
    public void callout2(){       
        IntegrationWrapperClass_CF iw = IntegrationClass_CF.synchronousCallout('sampleend');
        system.debug(iw.strResponseBody);
        if (iw.intResponseStatusCode == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            List<Account> accstr=(List<Account>)JSON.deserializeStrict(iw.strResponseBody, List<Account>.Class);
            System.debug('accstr '+accstr );
            //  list<account> acc=new list<account>();
            acc.addAll(accstr);
            System.debug('acc value '+acc );
            //setCon = new ApexPages.StandardSetController(acc);
            //System.debug('setCon value'+setCon);
            oneele=acc.get(0);
            System.debug('size '+acc.size());
        }
    }
}