public with sharing class TesterController {

    

    public TesterController(ApexPages.StandardController controller) {

    }

    public TesterController() {
        String[] payment= new String[]{'credit card','Debit card','netbanking'};
        this.modeofpayment= new SelectOption[]{};
        
        for (String c: payment) {
            this.modeofpayment.add(new SelectOption(c,c));
        }
    }
    public SelectOption[] modeofpayment { //this is where we're going to pull the list
        public get;
        private set;
    }
}