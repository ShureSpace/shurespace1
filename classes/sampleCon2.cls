public class sampleCon2{

    public sampleCon2() {
    setinternal(true);
            setexternal(False);
            setalignment(false);

    }

    public sampleCon2(ApexPages.StandardController controller) {

    }
     
    //To store the picklist value which we have selected on VF page i.e (Ind, Aus, USA)
    public String selectview{ get; set; }    
    
    //To display list of picklist values on VF page
        public List<selectOption> getitems() {
        List<selectOption> options = new List<selectOption>(); 
         options.add(new selectOption('internal','internal'));      
        options.add(new selectOption('external','external'));
        options.add(new selectOption('alignment','alignment'));
        return options;
        }
    public Boolean setint1= false;
    public Boolean setext1= false;
    public Boolean setali1= false;   
  
    
     public void setinternal(Boolean a) {
        this.setint1= a;
    }
    public Boolean getinternal() {
        return this.setint1;
    }    
    
    public void setexternal(Boolean a) {
        this.setext1= a;
    }
    public Boolean getexternal() {
        return this.setext1;
    }    
    
    
    public void setalignment(Boolean a) {
        this.setali1= a;
    }
    public Boolean getalignment() {
        return this.setali1;
    }   
    public pagereference paymentmode1()
    {
    if(selectview== 'external') {
            setinternal(false);
            setexternal(true);
            setalignment(False);
        }
   
        else if(selectview== 'internal')  {
         setinternal(true);
            setexternal(False);
            setalignment(false);
        
        
        }
        else   {
         setinternal(False);
            setexternal(False);
            setalignment(true);
        
        
        }
        return null;
    }
    
}