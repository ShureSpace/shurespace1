public class searchit {
@AuraEnabled
    public static list<Account> getaccounts(string accname){
        list<Account> alist=new list<Account>();
        alist=[select name,id,Location__longitude__s,Location__latitude__s  from Account where name =:accname limit 1];
   
        return alist;
    }
    
}