public class KnowledgeArticleclass_ACE {
    @auraEnabled
    public static list<object> getarticles(string strCaseComments){
        String strCommentsToFind;
        String strErrorNumber;
        KnowledgeArticleWrapper_ACE objKnowledgeArticleWrapper_ACE;
        KnowledgeArticleResponseWrapper_ACE objKnowledgeArticleResponseWrapper;
        List<String> lstCommentsTokens;
        List<List<sObject>> lstSearchResults;
        List<KnowledgeArticleWrapper_ACE> lstKnowledgeArticleWrapper;
        List<ErrorLog_CF__c> lstErrorLog;

        strCaseComments = strCaseComments.trim();
            lstCommentsTokens = strCaseComments.split(' ');
            strCommentsToFind = String.join(lstCommentsTokens, '* OR ');
            strCommentsToFind = '\'' + strCommentsToFind + '*\'';
            
            //Doing SOSL query.
            lstKnowledgeArticleWrapper = new List<KnowledgeArticleWrapper_ACE>();
            lstSearchResults = [FIND :strCommentsToFind IN ALL FIELDS RETURNING Knowledge__kav (Id, Title, Summary, PublishStatus WHERE PublishStatus = 'online' ORDER BY CreatedDate DESC) LIMIT 3];
return lstSearchResults;			
        
    }	

}