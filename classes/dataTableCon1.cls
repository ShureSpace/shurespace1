public with sharing class dataTableCon1 {

    public Integer totalPrice {get;set;} 
    public String userAddress{get;set;}
    public String typeOfPayment{get;set;}
    public My_Order__c deliveryOrder{get;set;}
    public List<myWrapper> lstmyWrapper=new List<myWrapper>();
    
    public dataTableCon1(){
        totalPrice =0;
        deliveryOrder=new My_Order__c();
        
    }
    
    public PageReference Payment() {
        return null;
    }
    
   
 
    public List<myWrapper> getWrapperList(){
         
        List<Inventory__c> lstProdInvt=[select Name,Price__c,Discount__c from Inventory__c];
        for(Inventory__c eachItem:lstProdInvt){
            myWrapper objWrap=new myWrapper();
            objWrap.ProdInvt=eachItem;
            lstmyWrapper.add(objWrap);
        }
        return lstmyWrapper;
    }
    
       
    public PageReference Proceed() {
        for(myWrapper eachwrap:lstmyWrapper){
          if(eachwrap.checked){
            totalPrice+=eachwrap.quantity*Integer.valueOf(eachwrap.ProdInvt.Price__c);
          }  
        }
        List<user> currentUser=[select id,Address from user where Id=:UserInfo.getUserID()];
        if(currentUser.size()>0){
            userAddress=currentUser[0].Address.getState();
        }
        
        return page.GroceryOrderVF;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('COD','COD'));
        options.add(new SelectOption('Debit Card','Debit Card'));
        options.add(new SelectOption('Net Banking','Net Banking'));
        return options;
    }
    
    public class myWrapper{
        public Boolean checked{get;set;}
        public Integer quantity{get;set;}
        public Inventory__c ProdInvt{get;set;}
    }
}