public with sharing class cla{
    @AuraEnabled
    public static list<Account> getdetails(String acc1){
        return [select Name,Phone from Account where name like :acc1+'%'];
    }
    @AuraEnabled
    public static Account saveAccount (Account acc) {
        System.debug('Account ::'+acc);
        
        insert acc;
        return acc;
    }
 
 
  @AuraEnabled
    public static List<String> getIndustry(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.Industry.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }

}