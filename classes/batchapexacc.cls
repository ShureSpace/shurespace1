global class batchapexacc implements Database.Batchable<sobject>, Database.Stateful {
    List<Contact> contlist = new List<Contact>();
    Set<Contact> contactSet = new Set<Contact>();
    global database.QueryLocator start(database.BatchableContext BC){
        string query='select name,id,Status__c from account limit 10';
        return database.getQueryLocator(query);
    }    
    
    global void execute(database.BatchableContext Bc,List<account> acclis){
       
        
        system.debug('acclis:'+acclis);
        list<account> alist1=new list<account>();
        for(account a:acclis){
            if(a.Status__c=='open'){
                system.debug(a);
                alist1.add(a);
            }     
            system.debug('alist1:'+alist1);

            list<contact> con =[select AccountId,status__c,Email from contact where AccountId IN :alist1 limit 10];
            system.debug('Contact size:'+con.size());
           // contactSet.addAll(con);
            for(contact c:con){
                c.status__c='open';
                contactSet.add(c);
                
            }
        }
        contlist.addAll(contactSet);
        System.debug('contact to update:'+contlist);
        update contlist;
          
    }
    
    global void finish(Database.BatchableContext BC){
         list<Messaging.SingleEmailMessage> mail = new list<Messaging.SingleEmailMessage>();
        System.debug('size of contlist-->'+contlist.size());
        for(contact c:contlist){
                        Messaging.SingleEmailMessage objEmail = new Messaging.SingleEmailMessage();
                list<string> toadd=new list<string>();
                toadd.add(c.Email);
                system.debug('*****'+c.Email);
                objEmail.settoaddresses(toadd);
                system.debug('*****'+toadd);
                objEmail.setsubject('notification');
                objEmail.setplaintextbody('your account has been updated '+c.Firstname);
                mail.add(objEmail); 
        }
        Messaging.SendEmailResult[]  result =Messaging.sendEmail(mail); 
       // contactSet.addAll(contlist);
       // System.debug('size of contactSet '+contactSet.size());
        batchapex2 b=new batchapex2(contlist);
        database.executeBatch(b);
       	System.debug(BC);
    }
    
}