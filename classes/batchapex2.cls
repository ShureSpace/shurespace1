global class batchapex2 implements Database.Batchable<sobject>, Database.Stateful{
    List<COntact> contactList = new List<Contact>();
    public batchapex2(List<Contact> lstCon){
        system.debug('lstCon-->'+lstCon);
		this.contactList = lstCon;
       	system.debug('contactList-->'+contactList);
    }
    
    global database.QueryLocator start(database.BatchableContext BC){
        string query='select name,id,countOfContacts__c from apple__C limit 10';
        return database.getQueryLocator(query);
    }  
    global void execute(database.BatchableContext Bc,List<Apple__c> appleList){
        
        for(apple__c apple : appleList){
            List<Contact> countList = [SELECT Id, Apple__c FROM Contact where Apple__r.Id = :apple.Id and Id IN :contactList];
            system.debug(countList.size());
            system.debug(contactList.size());
            if(countList.size() > 0 && !countList.isEmpty()){
                
			apple.countOfContacts__c = countList.size();
                system.debug(countList.size());
            }
            system.debug(apple);
            update apple;
        }
        
        }
    
     global void finish(database.BatchableContext Bc){
        
    }

}