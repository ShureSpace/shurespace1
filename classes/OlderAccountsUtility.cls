public class OlderAccountsUtility {
    public static void updateOlderAccounts(){
        account[] oldaccounts=[select name,id from account ORDER BY CreatedDate asc LIMIT 5  ];
        FOR(account acc: oldaccounts){
            acc.Description='Heritage Account';
        }
        update oldaccounts;
    }
}