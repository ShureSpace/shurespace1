/**
 *This is wrapper class for matching contact.
 *<p/><p/>
 *@author Sachin
 */
public class KnowledgeArticleWrapper_ACE {
    
    //Getters and Setters.
    @AuraEnabled public String strTitle {get;set;}
    @AuraEnabled public String strKnowledgeArticleId {get;set;}
    @AuraEnabled public String strSummary {get;set;}
    
}