public class TeamCtl {
    
    @AuraEnabled
    public static List<Contact> searchTeamMember(String searchTerm, String accountId, String selectedMembers) {       
        List<Contact> contactList = new List<Contact>();        
        if(String.isNotBlank(searchTerm)){
            Set<String> existingMembers = new Set<String>();
            if(String.isNotBlank(selectedMembers))
                existingMembers.addAll(selectedMembers.split(','));
            
            String tempInput = searchTerm + '%';
            contactList = [SELECT Id, Name FROM Contact 
                           WHERE AccountId = :accountId
                           AND Id NOT IN :existingMembers
                           AND  (Name LIKE : tempInput OR LastName LIKE :tempInput)
                           ORDER BY Name ASC LIMIT 5];
        }
		return contactList;       
    }
    
    @AuraEnabled    
    public static String saveTeam(String teamName, String accountId, String selectedMembers){        
        String retVal = 'Error';
        //Set Save point
        Savepoint sp = Database.setSavepoint();       
        try{
        	//Create Team
            Team__c team = new Team__c(); 
            if(String.isNotBlank(teamName))
                team.Name = teamName;
            if(String.isNotBlank(accountId))
                team.Account__c = accountId;
            insert team;
            //Create Teammember
            List<Team_Member__c> tmList = new List<Team_Member__c>();
            if(String.isNotBlank(selectedMembers)){
                for(String contactId: selectedMembers.split(',')){
                    Team_Member__c tm = new Team_Member__c(Team__c = team.Id,
                                                           Contact__c = contactId);
                    tmList.add(tm);
                }
                insert tmList; 
            }
			retVal = team.Id;
        }catch(Exception ex){          
            System.debug('Exception: ' + ex);
        }
        return retVal;
    }
    
    /**
     * Wrapper Class for Team Member
     */
    public class TeamMember{
		@AuraEnabled
        public Boolean isSelected {get; set;}
        public String recId		  {get; set;}
        public String name		  {get; set;}        
        public TeamMember(){}
    }
}