public with sharing class  accountlist {
    @AuraEnabled
    public static List < account > fetchAccount(string searchKeyWord, String industry){
        SYSTEM.debug('==============searchKeyWord:'+searchKeyWord);
       // SYSTEM.debug('==============date1:'+date1);
        SYSTEM.debug('==============industry:'+industry);
        //String todate = date1;
        // date d=date.valueOf(date1);
        //system.debug(d);
        string searchkey=searchKeyWord+'%';
        List < Account > returnList = new List < Account > ();
        List < Account > lstOfAccount = [select id, Name, Type, Industry, Phone,SLAExpirationDate__c,Fax from account where Name LIKE: searchKeyWord and Industry =: industry ];
       // List < Account > lstOfAccount = [select id, Name, Type, Industry, Phone,SLAExpirationDate__c,Fax from account where Name LIKE: searchKey and Industry LIKE:industry AND SLAExpirationDate__c =: d ];
        
        for (Account acc: lstOfAccount) {
            returnList.add(acc);
        }
        return returnList;
    }
}