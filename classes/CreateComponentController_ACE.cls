public class CreateComponentController_ACE {
/**
 *Lightning component controller for case creation and knowledge article display.
 *<p /><p />
 *@author Sachin
 */

    
    /**
     *This method returns case active status.
     * 
     *@return List<String> Return case status.
     */
    @AuraEnabled
    public static List<String> getCaseStatus() {
        List<String> lstCaseStatus = new List<String>();
        for(Schema.PicklistEntry objPicklistEntry : Case.Status.getDescribe().getPicklistValues()) {
            lstCaseStatus.add(objPicklistEntry.getValue());
        }
        return lstCaseStatus;
    }
    
    /**
     *This method returns case active origin.
     * 
     *@return List<String> Return case origin.
     */
    @AuraEnabled
    public static List<String> getCaseOrigin() {
        List<String> lstCaseOrigin = new List<String>();
        for(Schema.PicklistEntry objPicklistEntry : Case.Origin.getDescribe().getPicklistValues()) {
            lstCaseOrigin.add(objPicklistEntry.getValue());
        }
        return lstCaseOrigin;
    }
    
    /**
     *This method creates case.
     *
     *@Param strStatus. Hold's the Case status value selected by the user.
     *@Param strOrigin. Hold's the Case origin.
     *@Param strComments. Hold's the Case comments entered by the user.
     *@Param idContactId. Hold's the Contact record Id on which the Case is being created.
	 *@return DMLOperationResultClass_CF. Returns case creation response with error number(if any).
     */
    @AuraEnabled
    public static CreateCaseResponseWrapper_ACE createCase(String strStatus, String strOrigin, String strComments, Id idContactId) {
        Boolean boolDMLInsertResult;
        String strErrorNumber;
        CreateCaseResponseWrapper_ACE objCreateCaseResponseWrapper;
        DMLOperationResultClass_CF objDMLOperationCaseInsertResultClass;
        DMLOperationResultClass_CF objDMLOperationCaseCommentInsertResultClass;
        List<Case> lstCaseToInsert;
        List<CaseComment> lstCaseCommentToInsert;
        List<ErrorLog_CF__c> lstErrorLog;
        
        //Inserting case record.
        lstCaseToInsert = new List<Case>();
        lstCaseToInsert.add(new Case(Status = strStatus, Origin = strOrigin, ContactId = idContactId));
        objDMLOperationCaseInsertResultClass = DMLOperationsClass_CF.insertRecords(lstCaseToInsert);
        
        lstCaseCommentToInsert = new List<CaseComment>();
        if(objDMLOperationCaseInsertResultClass != null) {
            for(DMLResultClass_CF objDMLResultClass : objDMLOperationCaseInsertResultClass.lstResults) {
                if(objDMLResultClass.boolIsSuccess && !String.isEmpty(objDMLResultClass.objId)) {
                    lstCaseCommentToInsert.add(new CaseComment(ParentId = objDMLResultClass.objId, CommentBody = strComments));
                }
            }
        }
        
        //Inserting case comment record.
        boolDMLInsertResult = false;
        if(!lstCaseCommentToInsert.isEmpty()) {
            objDMLOperationCaseCommentInsertResultClass = DMLOperationsClass_CF.insertRecords(lstCaseCommentToInsert);
            for(DMLResultClass_CF objDMLResultClass : objDMLOperationCaseCommentInsertResultClass.lstResults) {
                boolDMLInsertResult = objDMLResultClass.boolIsSuccess;
            }
        }
        
        if(boolDMLInsertResult) {
            objCreateCaseResponseWrapper = new CreateCaseResponseWrapper_ACE();   
            objCreateCaseResponseWrapper.boolSuccess = true;
            objCreateCaseResponseWrapper.strErrorNumber = null;
        } else {
            //Fetching the error number to show in UI as per the wireframe.
            lstErrorLog = [SELECT Name FROM ErrorLog_CF__c WHERE CreatedById =: UserInfo.getUserId() ORDER BY CreatedDate DESC LIMIT 1];
            for(ErrorLog_CF__c objErrorLog : lstErrorLog) {
                strErrorNumber = objErrorLog.Name;
            }
            objCreateCaseResponseWrapper = new CreateCaseResponseWrapper_ACE();   
            objCreateCaseResponseWrapper.boolSuccess = false;
            objCreateCaseResponseWrapper.strErrorNumber = strErrorNumber;
        }
        return objCreateCaseResponseWrapper;
    }
    
    /**
     *This method searches knowledge article.
     * 
     *@param strCaseComments. Hold's the Case comments entered by the user.
     *@return List<String>. Returns knowledge article's related to the case comment's as response.
     */ 
    @AuraEnabled
    public static KnowledgeArticleResponseWrapper_ACE getMatchingKnowledgeArticle(String strCaseComments) {
        String strCommentsToFind;
        String strErrorNumber;
        KnowledgeArticleWrapper_ACE objKnowledgeArticleWrapper_ACE;
        KnowledgeArticleResponseWrapper_ACE objKnowledgeArticleResponseWrapper;
        List<String> lstCommentsTokens;
        List<List<sObject>> lstSearchResults;
        List<KnowledgeArticleWrapper_ACE> lstKnowledgeArticleWrapper;
        List<ErrorLog_CF__c> lstErrorLog;
        
        try {
            //Creating SOSL string to find.
            strCaseComments = strCaseComments.trim();
            lstCommentsTokens = strCaseComments.split(' ');
            strCommentsToFind = String.join(lstCommentsTokens, '* OR ');
            strCommentsToFind = '\'' + strCommentsToFind + '*\'';
            
            //Doing SOSL query.
            lstKnowledgeArticleWrapper = new List<KnowledgeArticleWrapper_ACE>();
            lstSearchResults = [FIND :strCommentsToFind IN ALL FIELDS RETURNING Knowledge__kav (Id, Title, Summary, PublishStatus WHERE PublishStatus = 'online' ORDER BY CreatedDate DESC) LIMIT 3];
            for(List<sObject> lstSobj : lstSearchResults) {
                for(Knowledge__kav objKnowledge : (List<Knowledge__kav>) lstSobj) {
                    objKnowledgeArticleWrapper_ACE = new KnowledgeArticleWrapper_ACE();
                    objKnowledgeArticleWrapper_ACE.strTitle = objKnowledge.Title;
                    objKnowledgeArticleWrapper_ACE.strKnowledgeArticleId = objKnowledge.Id;
                    objKnowledgeArticleWrapper_ACE.strSummary = objKnowledge.Summary;
                    lstKnowledgeArticleWrapper.add(objKnowledgeArticleWrapper_ACE);
                }
                objKnowledgeArticleResponseWrapper = new KnowledgeArticleResponseWrapper_ACE();
                objKnowledgeArticleResponseWrapper.boolSuccess = true;
                objKnowledgeArticleResponseWrapper.strErrorNumber = null;
                objKnowledgeArticleResponseWrapper.lstKnowledgeArticleWrapper = lstKnowledgeArticleWrapper;
            }
        } catch(Exception objException) {
            ErrorLogClass_CF.processException(objException);
            
            //Fetching the error number to show in UI as per the wireframe.
            lstErrorLog = [SELECT Name FROM ErrorLog_CF__c WHERE CreatedById =: UserInfo.getUserId() ORDER BY CreatedDate DESC LIMIT 1];
            for(ErrorLog_CF__c objErrorLog : lstErrorLog) {
                strErrorNumber = objErrorLog.Name;
            }
            objKnowledgeArticleResponseWrapper = new KnowledgeArticleResponseWrapper_ACE();
            objKnowledgeArticleResponseWrapper.boolSuccess = false;
            objKnowledgeArticleResponseWrapper.strErrorNumber = strErrorNumber;
            objKnowledgeArticleResponseWrapper.lstKnowledgeArticleWrapper = lstKnowledgeArticleWrapper;            
        } 
        return objKnowledgeArticleResponseWrapper;
    }
}