/**
*Controller class for DirectContinuation_ACE. This class makes call out using Continuation.
*<p /><p />
*@author Pramod Reddy
*/
public without sharing class DirectContinuationController_ACE {  
    
    public IntegrationWrapperClass_CF objIntegration = new IntegrationWrapperClass_CF();
    public List<ContinuationResponseWrapper_ACE> lstContinuationResponseWrapper {get;set;}
    
    /**
     *This method hit the service using continuation call out.
     */
    public Object getDataFromService() {        
        objIntegration = IntegrationClass_CF.continuationCallout('sampleend', 'continuationCallback');
        HttpRequest objHttpRequest = new HttpRequest();
        objHttpRequest.setMethod(objIntegration.strRequestMethod);
        objHttpRequest.setEndpoint(objIntegration.strRequestEndpoint);
        Continuation objContinuation = new Continuation(120);//objIntegration.intTimeout);
        objIntegration.strRequestKey = objContinuation.addHttpRequest(objHttpRequest);
        objContinuation.continuationMethod = objIntegration.strCallbackMethodName;
        return objContinuation;
    }
    
    /**
     *Continuation callback method.
     *
     *@return null to render vf page.
     */
    public object continuationCallback() {
        lstContinuationResponseWrapper = new List<ContinuationResponseWrapper_ACE>();
        objIntegration = IntegrationClass_CF.getContinuationResponse(objIntegration);
        if(!String.isBlank(objIntegration.strResponseBody)) {
            List<Object> lstResponseBody = (List<Object>)JSON.deserializeUntyped(objIntegration.strResponseBody);
            for(Object objElement : lstResponseBody) {
                Map<String, Object> mapResponseElementBody = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(objElement));
                ContinuationResponseWrapper_ACE objContinuationResponseWrapper = new ContinuationResponseWrapper_ACE();
                objContinuationResponseWrapper.strId = String.valueOf(mapResponseElementBody.get('id'));
                objContinuationResponseWrapper.strUserId = String.valueOf(mapResponseElementBody.get('userId'));
                objContinuationResponseWrapper.strTitle = String.valueOf(mapResponseElementBody.get('title'));
                objContinuationResponseWrapper.strBody = String.valueOf(mapResponseElementBody.get('body'));
                lstContinuationResponseWrapper.add(objContinuationResponseWrapper);
            }
        }
        return null;
    }
}