/** 
*This class serves as Test class for searchcontactclass_ACE visualforce page. 
*<p/><p/>
*@author Pramod
*/
@istest
public class SearchContactTest_ACE {
    /**
     * This method is used for the creting test data and calling apex class.
     */
    public static testMethod void searchtest(){
        searchcontactclass_ACE test=new searchcontactclass_ACE();
        test.createnew();
        test.callout2();
        test.getIsNewButtonDisabled();
        test.getIsSaveButtonDisabled();
        test.searchbox();
        test.res1='test';
    }
}