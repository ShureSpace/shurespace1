public class upload {
@auraEnabled
    public static Account getaccountdetails(string accname){
        account a=new account();
        a=[select name,id,AccountNumber from account where name=:accname LIMIT 1];
        return a;
    
    }
       @AuraEnabled
    public static Id saveThem(Id parentid, String filenamesr, String base64, String contentname) { 
        base64 = EncodingUtil.urlDecode(base64, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentid;
        a.Body = EncodingUtil.base64Decode(base64);
        a.Name = filenamesr;
        a.ContentType = contentname;
        
        insert a;
        
        return a.Id;
    }

}