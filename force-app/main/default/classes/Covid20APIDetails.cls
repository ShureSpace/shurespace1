public class Covid20APIDetails {
        public Covid20APIDetails(){

        }

        public class States_daily {
            public String an {get;set;} 
            public String ap {get;set;} 
            public String ar {get;set;} 
            public String as_Z {get;set;} // in json: as
            public String br {get;set;} 
            public String ch {get;set;} 
            public String ct {get;set;} 
            public String date1 {get;set;} 
            public String dd {get;set;} 
            public String dl {get;set;} 
            public String dn {get;set;} 
            public String ga {get;set;} 
            public String gj {get;set;} 
            public String hp {get;set;} 
            public String hr {get;set;} 
            public String jh {get;set;} 
            public String jk {get;set;} 
            public String ka {get;set;} 
            public String kl {get;set;} 
            public String la {get;set;} 
            public String ld {get;set;} 
            public String mh {get;set;} 
            public String ml {get;set;} 
            public String mn {get;set;} 
            public String mp {get;set;} 
            public String mz {get;set;} 
            public String nl {get;set;} 
            public String or_Z {get;set;} // in json: or
            public String pb {get;set;} 
            public String py {get;set;} 
            public String rj {get;set;} 
            public String sk {get;set;} 
            public String status {get;set;} 
            public String tg {get;set;} 
            public String tn {get;set;} 
            public String tr {get;set;} 
            public String tt {get;set;} 
            public String up {get;set;} 
            public String ut {get;set;} 
            public String wb {get;set;} 
    
            public States_daily(JSONParser parser) {
                while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                    if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                        String text = parser.getText();
                        if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                            if (text == 'an') {
                                an = parser.getText();
                            } else if (text == 'ap') {
                                ap = parser.getText();
                            } else if (text == 'ar') {
                                ar = parser.getText();
                            } else if (text == 'as') {
                                as_Z = parser.getText();
                            } else if (text == 'br') {
                                br = parser.getText();
                            } else if (text == 'ch') {
                                ch = parser.getText();
                            } else if (text == 'ct') {
                                ct = parser.getText();
                            } else if (text == 'date') {
                                date1 = parser.getText();
                            } else if (text == 'dd') {
                                dd = parser.getText();
                            } else if (text == 'dl') {
                                dl = parser.getText();
                            } else if (text == 'dn') {
                                dn = parser.getText();
                            } else if (text == 'ga') {
                                ga = parser.getText();
                            } else if (text == 'gj') {
                                gj = parser.getText();
                            } else if (text == 'hp') {
                                hp = parser.getText();
                            } else if (text == 'hr') {
                                hr = parser.getText();
                            } else if (text == 'jh') {
                                jh = parser.getText();
                            } else if (text == 'jk') {
                                jk = parser.getText();
                            } else if (text == 'ka') {
                                ka = parser.getText();
                            } else if (text == 'kl') {
                                kl = parser.getText();
                            } else if (text == 'la') {
                                la = parser.getText();
                            } else if (text == 'ld') {
                                ld = parser.getText();
                            } else if (text == 'mh') {
                                mh = parser.getText();
                            } else if (text == 'ml') {
                                ml = parser.getText();
                            } else if (text == 'mn') {
                                mn = parser.getText();
                            } else if (text == 'mp') {
                                mp = parser.getText();
                            } else if (text == 'mz') {
                                mz = parser.getText();
                            } else if (text == 'nl') {
                                nl = parser.getText();
                            } else if (text == 'or') {
                                or_Z = parser.getText();
                            } else if (text == 'pb') {
                                pb = parser.getText();
                            } else if (text == 'py') {
                                py = parser.getText();
                            } else if (text == 'rj') {
                                rj = parser.getText();
                            } else if (text == 'sk') {
                                sk = parser.getText();
                            } else if (text == 'status') {
                                status = parser.getText();
                            } else if (text == 'tg') {
                                tg = parser.getText();
                            } else if (text == 'tn') {
                                tn = parser.getText();
                            } else if (text == 'tr') {
                                tr = parser.getText();
                            } else if (text == 'tt') {
                                tt = parser.getText();
                            } else if (text == 'up') {
                                up = parser.getText();
                            } else if (text == 'ut') {
                                ut = parser.getText();
                            } else if (text == 'wb') {
                                wb = parser.getText();
                            } else {
                                System.debug(LoggingLevel.WARN, 'States_daily consuming unrecognized property: '+text);
                                //consumeObject(parser);
                            }
                        }
                    }
                }
            }
        }
        
        public List<States_daily> states_daily {get;set;}     
        
    }