public class pickcla {
    @AuraEnabled
    public static List<String> getIndustry(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.Industry.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
                   }
        
        return options;
    }
    @AuraEnabled
    public static list<account> saveAccount1 () {
      
        return [select Industry from Account ];
    }
    @AuraEnabled
    public static Account saveAccount (Account acc) {
        System.debug('Account ::'+acc);
        
        insert acc;
        return acc;
    }
 
}