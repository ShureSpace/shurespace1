public class HttpCalloutSample {

    public HttpCalloutSample(ApexPages.StandardController controller) {
    // Instantiate a new http object
        Http h = new Http();
        
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://media.giphy.com/media/l0MYyDa8S9ghzNebm/source.gif');
        req.setMethod('GET');
        
        // Send the request, and return a response
        HttpResponse res = h.send(req);
       // return res.getBody();
        }

    }