public with sharing class pramodextension {

    public Account acc = new Account();
    
    public pramodextension(ApexPages.StandardController controller) {
        acc = (Account)controller.getRecord();
    }
    
    public void updaterecord(){
        String accId = ApexPages.currentPage().getParameters().get('id') ;
        
        try{
            List<Account> accList = new List<Account>() ; 
            Account accobj = new Account() ;
            accobj.id =accId  ;
            accobj.Phone = acc.Phone; //acc.Phone ;
            accList.add( accobj ) ;
            
            update accList ;
        
        }catch( Exception e){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));  
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'special friendly message to user'));
           
           
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getMessage() );
            ApexPages.addMessage(msg);  
            //return null;          
        }
        
    }

}