public class accstatus {   
    //public  Id contactid;  
    public  Id accountid;
    list<sobject> so=new list<sobject>();
    
    public  void contactFieldsConcat(list<account> acclist){
        //contactid=conid;
        /*accountid=acclist;
list<account> alist=new List<account>();
string query ='select id, name,Status__c,SLAExpirationDate__c from account where Id=:accountid limit 1';
alist=database.query(query);*/
        list<account> acclistterminated = new list<account>();
        for(account a:acclist){
            if(a.Status__c=='terminated'){
                acclistterminated.add(a);
            }
            list<contact> conlist=new list<contact>();
            conlist =[select id, name,Email from contact where AccountId IN :acclistterminated];
            list<contact> conlistToupdate= new list<contact>();
            for(contact con :conlist){
                if(con.Email==null){
                    con.date__c= Date.today();
                    so.add(con);
                } 
            }
            
            list<case> clistToUpdate = new List<case>();
            list<case> clist =[SELECT ContactId,Status,close_date__c FROM Case WHERE AccountId IN :acclistterminated];
            system.debug(clist);
            for(case c:clist){
                if(c.close_date__c>=date.today()){
                    system.debug(c.Status);
                    c.Status='case closed';
                    system.debug(c.Status);
                    so.add(c);
                }
            }
            update so;
        }
        
    }
    
}