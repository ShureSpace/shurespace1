<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Product_Inv__c</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>GLOBAL GROCERY</label>
    <tabs>Product_Inv__c</tabs>
    <tabs>Seller__c</tabs>
    <tabs>My_order__c</tabs>
    <tabs>Inventory__c</tabs>
    <tabs>cart__c</tabs>
    <tabs>Wishlist__c</tabs>
    <tabs>Myorder2__c</tabs>
    <tabs>Suggestion__c</tabs>
    <tabs>Supplie__c</tabs>
    <tabs>Waypoint__c</tabs>
    <tabs>Amount__c</tabs>
    <tabs>candidate__c</tabs>
    <tabs>shopnow__c</tabs>
    <tabs>ShOPnOW</tabs>
    <tabs>photo__c</tabs>
    <tabs>Project__c</tabs>
    <tabs>Resource_new__c</tabs>
    <tabs>Issue__c</tabs>
    <tabs>Title__c</tabs>
    <tabs>save_To_Google_Drive</tabs>
    <tabs>dress__c</tabs>
    <tabs>QRcode</tabs>
    <tabs>SearchcontactTab_ACE</tabs>
    <tabs>case__c</tabs>
    <tabs>Item__c</tabs>
</CustomApplication>
