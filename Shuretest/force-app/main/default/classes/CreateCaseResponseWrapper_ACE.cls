/**
 *This is wrapper class for case creation response.
 *<p/><p/>
 *@author Sachin
 */
public class CreateCaseResponseWrapper_ACE {
    
    //Getters and Setters.
    @AuraEnabled public Boolean boolSuccess {get;set;}
    @AuraEnabled public String strErrorNumber {get;set;}
 
}