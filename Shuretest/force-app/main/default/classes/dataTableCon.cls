public with sharing class dataTableCon {

    public String deliveryOrder { get; set; }

    public String items { get; set; }

    public String typeOfPayment { get; set; }

    public String userAddress { get; set; }

    public String Product_Inventory { get; set; }

    public String WrapperList { get; set; }
}