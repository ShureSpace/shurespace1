@istest
public class testacctrigger {
    
    public static testMethod void testRunAs() {
        test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='pramod.reddy@cognizant.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12565221@testorg.com');
        
        System.runAs(u) {
            // The following code runs as user 'u' 
            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId()); 
            list<account> acclistterminated=new list<account>();
            
            list<account> alist=new list<account>();
            Account acc=new Account(Name='Pramod',status__c='terminated');
            alist.add(acc);
            
            insert alist;
            //system.debug(alist);
            list<contact> con1=new list<contact>();
            contact c1=new contact(lastname='reddy',email='pramodreddy@gmail.com',AccountId =alist[0].id);
            contact c2=new contact(lastname='reddy',email='',AccountId =alist[0].id);
            con1.add(c1);
            con1.add(c2);
            insert con1;
            list<case> cas=new list<case>();
            case c5=new case(close_date__c=date.today(),AccountId =alist[0].id);
            case c6=new case(close_date__c=null,AccountId =alist[0].id);
            cas.add(c5);
            cas.add(c6);
            insert cas;
            accstatus c54=new accstatus();
            c54.contactFieldsConcat(alist);  
        }
        test.stopTest();
    }
    public static testMethod void testRunAs1(){
        account anew=new account();
        anew.name='name';
        insert anew;
        contact c=new contact(lastname=anew.Name,AccountId =anew.id);
        insert c;
        case c58=new case(close_date__c=date.today(),AccountId =anew.id);
        insert c58;
        Account testAccount = new Account( Name = 'Test Account',status__c='open',id=anew.Id);
        update testAccount;
        
        contact con=new contact();
        con = [select AccountId, Name,date__c from contact where AccountId = :testAccount.Id];
        System.assertNotEquals(con.date__c, date.today());
        
        Account testAccount1 = new Account( Name = 'Test Account',status__c='terminated',id=anew.Id);
        update testAccount1;
        contact con5=new contact();
        con5 = [select AccountId, Name,date__c from contact where AccountId = :testAccount.Id];
        System.assertEquals(con5.date__c, date.today());
        case cas1=new case();
        cas1 = [select AccountId,Status from case where AccountId = :testAccount.Id];
        System.assertEquals(cas1.Status, 'case closed'); 
          
    }
}