/**
 *Controller class for VFRemotingContinuation_ACE. This class makes call out using Continuation.
 *<p /><p />
 *@author Pramod Reddy
 */
public without sharing class VFRemotingContinuationController_ACE {  
    
    private static IntegrationWrapperClass_CF objIntegration = new IntegrationWrapperClass_CF();
    
    /**
     *This method hit the service using continuation call out.
     */
    @RemoteAction
    public static Object getDataFromService() {  
        objIntegration = IntegrationClass_CF.continuationCallout('sampleend', 'continuationCallback');
        HttpRequest objHttpRequest = new HttpRequest();
        objHttpRequest.setMethod(objIntegration.strRequestMethod);
        objHttpRequest.setEndpoint(objIntegration.strRequestEndpoint);
        Continuation objContinuation = new Continuation(60);//objIntegration.intTimeout);
        objContinuation.state = objContinuation.addHttpRequest(objHttpRequest); 
        objContinuation.continuationMethod = objIntegration.strCallbackMethodName;
        return objContinuation;
    }
    
    /**
     *Continuation callback method.
     *
     *@param objState holds state object.
     *@return response body.
     */
    public static object continuationCallback(Object objState) {
        objIntegration.strRequestKey = (String)objState;
        objIntegration = IntegrationClass_CF.getContinuationResponse(objIntegration);
        return objIntegration.strResponseBody;
    }    
}