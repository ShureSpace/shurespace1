public with sharing class payment1 {

    public payment1(ApexPages.StandardSetController controller) {
  

    }
    

    public payment1(ApexPages.StandardController controller) {

    }

      public payment1() {
      setsel(true);
      setcre(false);
      setdeb(false);
      setWAL(false);
        
    } 

     
    //To store the picklist value which we have selected on VF page i.e (Ind, Aus, USA)
    public String paymentype{ get; set; }    
    
    //To display list of picklist values on VF page
        public List<selectOption> getpaymentoptions() {
        List<selectOption> options = new List<selectOption>(); 
         options.add(new selectOption('sel','select card type'));      
        options.add(new selectOption('cre','Credit card'));
        options.add(new selectOption('deb','Debit Card'));
        options.add(new selectOption('WAL','Wallets'));
        return options;
        }
        public Boolean a=false;
    public Boolean cretf = false;
    public Boolean debtf= false;
    public Boolean waltf= false;   
    public Boolean seltf= false;   
    
     public void setsel(Boolean b) {
        this.seltf= b;
    }
    public Boolean getsel() {
        return this.seltf;
    }    
    
    public void setcre(Boolean b) {
        this.cretf = b;
    }
    public Boolean getcre() {
        return this.cretf;
    }    
    
    
    public void setdeb(Boolean b) {
        this.debtf= b;
    }
    public Boolean getdeb() {
        return this.debtf;
    }
 public void setWAL(Boolean b) {
        this.waltf= b;
    }
    public Boolean getWAL() {
        return this.waltf;
    }
  
    public void button(){
    Boolean a=true;
    }
    public pagereference paymentmode1()
    {
    if(paymentype== 'cre') {
            setcre(true);
            setdeb(False);
            setWAL(False);
        }
    else if(paymentype== 'deb') {
            setcre(false);
            setdeb(true);
            setWAL(False);
        }
        else {
            setcre(False);
            setdeb(False);
            setWAL(True);
        }
        return null;
    }
    
}