/**
*This call will is used to create the class and retrive the Article methods
*<p /><p />
*@author Pramod Reddy
*/

public class caseLightningComponentController_ACE {
    /**
     *This method is used for creating the case.
     *
     *@param conId is the contactid.
     *@param status1 is the status value comming from UI.
     *@param orgin1 is the case orgin comming from UI.
     *@param comment1 is the comment comming from UI.
     */
    @auraEnabled
    public static void  createcase(id conId,string status1,string orgin1,string comment1){
        Case caseObj = new Case(ContactId = conID,Status = status1,Origin = orgin1,Description= comment1);
        List<sobject> caselist=new List<sobject>();
        caselist.add(caseObj);
        DMLOperationsClass_CF.insertRecords(caselist);
    }
    /**
     *This method is used to perform SOSL on Articles using comments
     *
     *@param comment1 of type String comming from.
     *@retun Return lstSearchResults of type list.
     */
    @auraEnabled
    public static list<object> getarticles(string comment1){
        String strCommentsToFind;
        String strErrorNumber;
        KnowledgeArticleWrapper_ACE objKnowledgeArticleWrapper_ACE;
        KnowledgeArticleResponseWrapper_ACE objKnowledgeArticleResponseWrapper;
        List<String> lstCommentsTokens;
        List<List<sObject>> lstSearchResults;
        List<KnowledgeArticleWrapper_ACE> lstKnowledgeArticleWrapper;
        List<ErrorLog_CF__c> lstErrorLog;
        comment1 = comment1.trim();
        lstCommentsTokens = comment1.split(' ');
        strCommentsToFind = String.join(lstCommentsTokens, '* OR ');
        strCommentsToFind = '\'' + strCommentsToFind + '*\'';
        //Doing SOSL query.
        lstKnowledgeArticleWrapper = new List<KnowledgeArticleWrapper_ACE>();
        lstSearchResults = [FIND :strCommentsToFind IN ALL FIELDS RETURNING Knowledge__kav (Id, Title, Summary, PublishStatus WHERE PublishStatus = 'online' ORDER BY CreatedDate DESC) LIMIT 3];
        return lstSearchResults;			
    }
}