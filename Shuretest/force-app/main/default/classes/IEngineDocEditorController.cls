public class IEngineDocEditorController{
    public string docid{set;get;}
    public String inf{
        get {
            Document sr = [ select name,id,Body  from Document where Name = 'lightning inferences' ];
            docid=sr.id;
            return sr.Body.toString();
        }
        set{
            inf=value;
            blob b=Blob.valueof(inf);
            document doco=new document(id=docid);
            doco.Body=  b;
            update doco;   
        }
    }
    public void save(){
        system.debug('>>>>>>>>inf>>>>>>>>>>>'+inf);
    }
}