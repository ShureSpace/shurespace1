public class OpportunityHistoryCtl1 {	    
    @AuraEnabled
    public static List<OpportunityHistory> getOpportunityHistory (String recordId) {     
        List<OpportunityHistory> oppHistoryList = new List<OpportunityHistory>();
        if(String.isNotBlank(recordId)){
            oppHistoryList = [SELECT CreatedDate, OpportunityId, StageName FROM 
                              OpportunityHistory WHERE OpportunityId =:recordId
                              ORDER BY CreatedDate];
        }       
        return  oppHistoryList;
    }

}