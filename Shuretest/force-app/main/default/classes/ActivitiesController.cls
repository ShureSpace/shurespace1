public with sharing class ActivitiesController {
    public PageReference redirect() {
        return new PageReference('one/one.app#/sObject/Mobile_orders__c/new?count=1');
    }
}