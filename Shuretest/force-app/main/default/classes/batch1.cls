global class batch1 implements Database.Batchable<sObject>, Database.Stateful {
    global database.QueryLocator start(database.BatchableContext Bc)
    {
        string str='select id,name from account';
        return database.getQueryLocator(str);
    }
    global void execute(database.BatchableContext Bc,List<account> acclis){
        for(account a:acclis){
      		a.name=a.name+'hello ';
        }
       	update acclis;
    }
    global void finish(database.BatchableContext Bc){
        
    }
}