global class timmerClass {
    @AuraEnabled
    global static Integer timmerClass1() {
        User u = [select Id, username, DateOfJoining__c from User where Id = :UserInfo.getUserId()];
        //date date = u.DateOfJoining__c;   
        Integer dt1 = (Date.valueOf( u.DateOfJoining__c)).daysBetween(system.today());
        system.debug('+++++'+dt1);
        return dt1;

    }
}