/**
 *This is the test class for caseLightningComponentController_ACE class.
 *<p /><p />
 *@author Pramod 
 */

@istest
public class CaseCreationTest_ACE {
/**
 *This method will declear the class and cover the codecoverage which is 100%.
 */
     public static testMethod void searchtest(){
         contact c=new contact();
    c.lastname='test1';
    insert c;
    string status1='testnew';
    string orgin1='test1new';
    string comment1='testcomment';
    string strCaseComments='testcomments';
    id conId=c.Id;
        caseLightningComponentController_ACE acc=new caseLightningComponentController_ACE();
         caseLightningComponentController_ACE.createcase(conId, status1, orgin1, comment1);
         caseLightningComponentController_ACE.getarticles(strCaseComments);
    }
}